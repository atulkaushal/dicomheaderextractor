import ccf.nrg.siemens.dicomheaderextractor.SiemensDicomHeaderExtractor
import org.nrg.xdat.om.XnatExperimentdata
import org.nrg.xdat.om.XnatImagescandata
import groovy.io.FileType
import org.nrg.xdat.om.XnatAddfield
import org.nrg.xdat.bean.XnatImagescandataBean
import org.nrg.xft.utils.SaveItemHelper
import org.nrg.xft.XFTItem
import org.nrg.xdat.om.XnatMrscandata

try {
    def dicomHeaderGen= new SiemensDicomHeaderExtractor()
    def experimentData = XnatExperimentdata.getXnatExperimentdatasById(dataId, user, false)
    def scans=experimentData.getSortedScans()
    for (scan in scans) 
    {
        if(scan instanceof XnatMrscandata)
        {
            scan.setParameters_acqtype("");
            scan.setParameters_scansequence("");
            scan.setParameters_seqvariant("");
            scan.setParameters_scanoptions("");
            scan.setParameters_subjectposition("");
            scan.setParameters_pixelbandwidth(null);
            scan.setParameters_inplanephaseencoding_direction("");
            scan.setParameters_inplanephaseencoding_rotation("");
            scan.setParameters_readoutsamplespacing("");
            scan.setParameters_echospacing(null);
            scan.setParameters_inplanephaseencoding_directionpositive("");
            scan.setParameters_diffusion_refocusflipangle("");
            scan.setScanner_softwareversion("");
            scan.setCoil("");
            scan.setFilenameuuid("");
            scan.setParameters_inplanephaseencoding_rotation("");
          	int size=scan.getParameters_addparam().size();
        	for(int i=size-1;i>=0;i--)
        	{   
            	try{
            		//Skip MultiEcho_TE attribute as they are set by default XNAT configuration.
            		if(!scan.getParameters_addparam().get(i).getName().startsWith("MultiEcho_TE"))
            		scan.getParameters_addparam().get(i).setAddfield("");
            	} catch (Exception e) {
                 	println (e)
            	}
    		}
    		SaveItemHelper.authorizedSave(scan.getItem(),user,false,false,null)
        }
    }
} catch (Exception e) {
     println (e)
}

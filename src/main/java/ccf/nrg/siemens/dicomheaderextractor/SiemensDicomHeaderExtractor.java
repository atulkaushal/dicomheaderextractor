package ccf.nrg.siemens.dicomheaderextractor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xnat.helpers.dicom.DicomHeaderDump;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVReader;
import ccf.nrg.siemens.dicomheaderextractor.util.DicomConstants;
import ccf.nrg.siemens.dicomheaderextractor.util.DicomHeaderUtility;

/**
 * The Class SiemensDicomHeaderExtractor.
 * @author Atul
 */
@XnatPlugin(value = "dicomHeaderExtractionPlugin",
name = "Dicom HeaderExtraction Plugin",
log4jPropertiesFile="/dicomExtractionLog4j.properties")
public class SiemensDicomHeaderExtractor {

	/** The logger. */
	public static Logger logger =  LoggerFactory.getLogger(SiemensDicomHeaderExtractor.class);

	/** The siemens key map. */
	static Map<String, String> siemensKeyMap = new LinkedHashMap<String, String>();

	static {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new InputStreamReader(
					SiemensDicomHeaderExtractor.class.getResourceAsStream("/SiemensHeaders.csv")));
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				siemensKeyMap.put(nextLine[0], nextLine[1]);
			}
		} catch (IOException e) {
			logger.info("Unable to load Siemens Headers from SiemensHeaders.csv" + e.getMessage());
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Unable to load SiemensHeaders.csv file");
					logger.error(e.getMessage());
					e.printStackTrace();
				}
		}
	}

	/**
	 * Gets the siemens dicom header info map.
	 *
	 * @param dcmDirPath
	 *            the dcm dir path
	 * @return the siemens dicom header info map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Map<String, String> getSiemensDicomHeaderInfoMap(String dcmDirPath) throws IOException {
		logger.debug("Extracting Dicom headers for the files present under "+dcmDirPath);
		Map<String, String> siemensValuesMap = new LinkedHashMap<String, String>();
		try {
			// Get all the files with .dcm extension
			File[] filesWithDCMExtension = new File(dcmDirPath).listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(".dcm");
				}
			});
			// Get all the files without any extension. Special use-case for UCLA files.
			File[] filesWithoutExtension = new File(dcmDirPath).listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return !name.endsWith(".dcm") && !name.endsWith(".xml");
				}
			});
			if (filesWithDCMExtension != null && filesWithDCMExtension.length > 0)
				processFile(filesWithDCMExtension[0].getPath(), siemensValuesMap);
			if (filesWithoutExtension != null && filesWithoutExtension.length > 0)
				processFile(filesWithoutExtension[0].getPath(), siemensValuesMap);

			logger.debug("Extracting information from all dicom files");
			// Process All dicom files for specific attributes
			if (filesWithDCMExtension != null)
				getAttributesFromAllFiles(filesWithDCMExtension, siemensValuesMap);
			if (filesWithoutExtension != null)
				getAttributesFromAllFiles(filesWithoutExtension, siemensValuesMap);

			//printDicomHeaders(siemensValuesMap);
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			logger.error(ExceptionUtils.getStackTrace(e));
			throw e;
		}
		logger.debug("Returning HeaderInfo Map");
		return siemensValuesMap;
	}

	/**
	 * Gets the attributes from all files.
	 *
	 * @param files
	 *            the files
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @return the attributes from all files
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void getAttributesFromAllFiles(File[] files, Map<String, String> siemensValuesMap)
			throws FileNotFoundException, IOException {
		Map<String, Integer> imageCommentsMap = new TreeMap<String, Integer>();
		Map<Integer, Integer> acquisitionVsInstanceNumberMap = new HashMap<Integer, Integer>();
		Boolean useAcquisitionNumber = true;
		for (int i = 0; i < files.length; i++) {
			Map<String, String> headerValuesMap = getAllHeadersMap(files[i].getPath());
			getDiffusionMaxBValue(siemensValuesMap, headerValuesMap);
			getImageCommentsValue(imageCommentsMap, headerValuesMap, acquisitionVsInstanceNumberMap,
					useAcquisitionNumber);
		}
		Map<Integer, String> newMap = new TreeMap<Integer, String>();
		for (Map.Entry<String, Integer> entry : imageCommentsMap.entrySet())
			newMap.put(entry.getValue(), entry.getKey());

		if (newMap.size() > 1) {
			for (Iterator<Integer> iterator = newMap.keySet().iterator(); iterator.hasNext();) {
				Integer key = iterator.next();
				siemensValuesMap.put("Image Comments[" + key + "]", newMap.get(key));
			}
			siemensValuesMap.put(DicomConstants.IMAGE_COMMENTS_KEY, "<b>View/Download XML to view image comments.</b>");
		}
		if (newMap.size() == 1)
			siemensValuesMap.put(DicomConstants.IMAGE_COMMENTS_KEY, newMap.entrySet().iterator().next().getValue());
	}

	/**
	 * Gets the diffusion max B value.
	 *
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @param headerValuesMap
	 *            the header values map
	 * @return the diffusion max B value
	 */
	private void getDiffusionMaxBValue(Map<String, String> siemensValuesMap, Map<String, String> headerValuesMap) {
		if ((siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY) == null || DicomConstants.SPACE_STRING
				.equals(siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY).trim()))
				&& headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG) != null) {
			siemensValuesMap.put(DicomConstants.DIFFUSION_MAXIMUM_B_KEY,
					Double.valueOf(headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG)).toString());
		} else if (siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY) != null
				&& headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG) != null
				&& Double.valueOf(siemensValuesMap.get(DicomConstants.DIFFUSION_MAXIMUM_B_KEY)) < Double
						.valueOf(headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG))) {
			siemensValuesMap.put(DicomConstants.DIFFUSION_MAXIMUM_B_KEY,
					Double.valueOf(headerValuesMap.get(DicomConstants.DIFFUSION_MAX_B_TAG)).toString());
		}
	}

	/**
	 * Gets the image comments value.
	 *
	 * @param imageCommentsMap
	 *            the image comments map
	 * @param headerValuesMap
	 *            the header values map
	 * @param acquisitionVsInstanceNumberMap
	 *            the acquisition vs instance number map
	 * @param useAcquisitionNumber
	 *            the use acquisition number
	 * @return the image comments value
	 */
	private void getImageCommentsValue(Map<String, Integer> imageCommentsMap, Map<String, String> headerValuesMap,
			Map<Integer, Integer> acquisitionVsInstanceNumberMap, boolean useAcquisitionNumber) {
		if (headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG) != null
				&& imageCommentsMap.get(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG)) == null) {
			/**
			 * Use Acquisition number for index if it is unique for every dicom file.
			 * Otherwise, use Instance number.
			 */
			if (useAcquisitionNumber) {
				Integer acqNumKey = Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG));
				if (acquisitionVsInstanceNumberMap.containsKey(acqNumKey)) {
					useAcquisitionNumber = false;
					replaceAcqNumWithInstanceNum(imageCommentsMap, acquisitionVsInstanceNumberMap);
				} else {
					acquisitionVsInstanceNumberMap.put(
							Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG)),
							Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG)));
				}
			}

			if (useAcquisitionNumber)
				imageCommentsMap.put(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG),
						Integer.valueOf(headerValuesMap.get(DicomConstants.ACQUISITION_NUMBER_TAG)));
			else
				imageCommentsMap.put(headerValuesMap.get(DicomConstants.IMAGE_COMMENTS_TAG),
						Integer.valueOf(headerValuesMap.get(DicomConstants.INSTANCE_NUMBER_TAG)));
		}
	}

	/**
	 * Replace acq num with instance num.
	 *
	 * @param imageCommentsMap
	 *            the image comments map
	 * @param acquisitionVsInstanceNumberMap
	 *            the acquisition vs instance number map
	 */
	private void replaceAcqNumWithInstanceNum(Map<String, Integer> imageCommentsMap,
			Map<Integer, Integer> acquisitionVsInstanceNumberMap) {
		for (Iterator<Integer> iterator = acquisitionVsInstanceNumberMap.keySet().iterator(); iterator.hasNext();) {
			Integer acqNumber = iterator.next();
			for (Iterator<String> iterator2 = imageCommentsMap.keySet().iterator(); iterator2.hasNext();) {
				String imageComment = iterator2.next();
				if (imageCommentsMap.get(imageComment).equals(acqNumber)) {
					imageCommentsMap.put(imageComment, acquisitionVsInstanceNumberMap.get(acqNumber));
				}
			}
		}
	}

	/**
	 * Process file.
	 *
	 * @param filePath
	 *            the file path
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void processFile(String filePath, Map<String, String> siemensValuesMap)
			throws FileNotFoundException, IOException {
		Map<String, String> headerValuesMap = getAllHeadersMap(filePath);
		if (headerValuesMap != null)
			getSiemensHeadersMap(headerValuesMap, siemensValuesMap);
	}

	/**
	 * Gets the all headers map.
	 *
	 * @param filePath
	 *            the file path
	 * @return the all headers map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	private Map<String, String> getAllHeadersMap(String filePath) throws IOException, FileNotFoundException {
		DicomHeaderDump d = new DicomHeaderDump(filePath);
		XFTTable xft = d.render();
		DicomHeaderUtility util = new DicomHeaderUtility();
		Map<String, String> headerValuesMap = util.createHeaderMap(xft);
		return headerValuesMap;
	}

	/**
	 * Gets the siemens headers map.
	 *
	 * @param headerValuesMap
	 *            the header values map
	 * @param siemensValuesMap
	 *            the siemens values map
	 * @return the siemens headers map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void getSiemensHeadersMap(Map<String, String> headerValuesMap, Map<String, String> siemensValuesMap)
			throws IOException {
		for (Iterator<String> iterator = siemensKeyMap.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();
			if (headerValuesMap.get(key) != null && !DicomConstants.EMPTY_STRING.equals(headerValuesMap.get(key))) {
				siemensValuesMap.put(
						siemensKeyMap.get(key).replaceAll(DicomConstants.SPACE_STRING, DicomConstants.UNDERSCORE),
						headerValuesMap.get(key));
			}
		}
	}

	/**
	 * Send notifications.
	 *
	 * @param subject
	 *            the subject
	 * @param message
	 *            the message
	 * @param emailAddress
	 *            the email address
	 */
	public void sendNotifications(String subject, String message, String[] emailAddress) {
		AdminUtils.sendUserHTMLEmail(subject, message, false, emailAddress);
	}
	
	/**
	 * Prints the dicom headers.
	 *
	 * @param valuesMap
	 *            the values map
	 */
	private void printDicomHeaders(Map<String, String> valuesMap) {
		for (Iterator<String> iterator = valuesMap.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();
			System.out.println(key + " :: " + valuesMap.get(key));
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String... args) throws Exception {
		SiemensDicomHeaderExtractor s = new SiemensDicomHeaderExtractor();
		s.getSiemensDicomHeaderInfoMap("C:\\Users\\Atul\\Desktop\\HCA6058162_V1_B\\scans\\18_dMRI\\DICOM");

	}
}
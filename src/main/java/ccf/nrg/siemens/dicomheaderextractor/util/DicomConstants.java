package ccf.nrg.siemens.dicomheaderextractor.util;

/**
 * The Class DicomConstants.
 *
 * @author Atul
 */
public class DicomConstants {

	/** The Constant NEW_LINE. */
	public static final String NEW_LINE = "\n";

	/** The Constant ASCCONV_END. */
	public static final String ASCCONV_END = "### ASCCONV END";

	/** The Constant ASCCONV_BEGIN. */
	public static final String ASCCONV_BEGIN = "### ASCCONV BEGIN";

	/** The Constant FMRI_EXTERNAL_INFO. */
	public static final String FMRI_EXTERNAL_INFO = "FmriExternalInfo";

	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";

	/** The Constant VALUE. */
	public static final String VALUE = "value";

	/** The Constant TAG2. */
	public static final String TAG2 = "tag2";

	/** The Constant TAG1. */
	public static final String TAG1 = "tag1";

	/** The Constant MR_PHOENIX_PROTOCOL. */
	public static final String MR_PHOENIX_PROTOCOL = "MrPhoenixProtocol";

	/** The Constant FMRI_VERSION_ICE_LINUX. */
	public static final String FMRI_VERSION_ICE_LINUX = "fMRI_Version_ICE_Linux";

	/** The Constant FMRI_VERSION_ICE_WIN32. */
	public static final String FMRI_VERSION_ICE_WIN32 = "fMRI_Version_ICE_Win32";

	/** The Constant FMRI_VERSION_SEQUENCE. */
	public static final String FMRI_VERSION_SEQUENCE = "fMRI_Version_Sequence";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT = "sGRADSPEC.alShimCurrent";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_4. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_4 = "sGRADSPEC.alShimCurrent[4]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_3. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_3 = "sGRADSPEC.alShimCurrent[3]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_2. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_2 = "sGRADSPEC.alShimCurrent[2]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_1. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_1 = "sGRADSPEC.alShimCurrent[1]";

	/** The Constant S_GRADSPEC_AL_SHIM_CURRENT_0. */
	public static final String S_GRADSPEC_AL_SHIM_CURRENT_0 = "sGRADSPEC.alShimCurrent[0]";

	/** The Constant ECHO_SPACING_SEC. */
	public static final String ECHO_SPACING_SEC = "Echo_Spacing_(sec)";

	/** The Constant BACKSLASH. */
	public static final String BACKSLASH = "\\";

	/** The Constant SIEMENS_GRADSPEC_L_OFFSET. */
	public static final String SIEMENS_GRADSPEC_L_OFFSET = "Siemens GRADSPEC lOffset";

	/** The Constant S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Z. */
	public static final String S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Z = "sGRADSPEC.asGPAData[0].lOffsetZ";

	/** The Constant S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Y. */
	public static final String S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_Y = "sGRADSPEC.asGPAData[0].lOffsetY";

	/** The Constant S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_X. */
	public static final String S_GRADSPEC_AS_GPA_DATA_0_L_OFFSET_X = "sGRADSPEC.asGPAData[0].lOffsetX";
	
	/** The Constant SPACE_STRING. */
	public static final String SPACE_STRING = " ";
	
	/** The Constant UNDERSCORE. */
	public static final String UNDERSCORE = "_";
	
	/** The Constant DIFFUSION_MAX_B_CODE. */
	public static final String DIFFUSION_MAX_B_TAG="(0019,100C)";
	
	/** The Constant IMAGE_COMMENTS_CODE. */
	public static final String IMAGE_COMMENTS_TAG="(0020,4000)";
	
	/** The Constant IMAGE_INSTANCE_ID_CODE. */
	public static final String INSTANCE_NUMBER_TAG = "(0020,0013)";
	
	public static final String ACQUISITION_NUMBER_TAG = "(0020,0012)";

	/** The Constant IMAGE_COMMENTS_KEY. */
	public static final String IMAGE_COMMENTS_KEY = "Image_Comments";

	/** The Constant DIFFUSION_MAXIMUM_B_KEY. */
	public static final String DIFFUSION_MAXIMUM_B_KEY = "Diffusion_maximum_b";
	
	/** The Constant READOUT_SAMPLE_SPACING_NUMBER_TAG. */
	public static final String READOUT_SAMPLE_SPACING_NUMBER_TAG = "(0019,1018)";
}
